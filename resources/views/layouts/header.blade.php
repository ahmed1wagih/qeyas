<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <!-- Meta information -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"><!-- Mobile Specific Metas -->

    <!-- Title -->
    <title>Home</title>

    <!-- favicon icon -->
    <link rel="shortcut icon" href="{{ asset('images/Favicon.ico') }}">

    <!-- CSS Stylesheet -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"><!-- bootstrap css -->
    <link href="{{ asset('css/bootstrap-rtl.min.css') }}" rel="stylesheet"><!-- bootstrap rtl css -->
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet"><!-- carousel Slider -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"><!-- font awesome -->
    <link href="{{ asset('css/loader.css') }}" rel="stylesheet"><!--  loader css -->
    <link href="{{ asset('css/docs.css') }}" rel="stylesheet"><!--  template structure css -->
    <link href="https://fonts.googleapis.com/css?family=Arima+Madurai:100,200,300,400,500,700,800,900%7CPT+Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Tajawal:500&display=swap" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="wapper">
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <div class="quck-nav">
            <div class="container">
                <div class="contact-no Tajawal-font"><i class="fa fa-calendar"></i>12 مايو 2019 - 22 ذي الحجة 1434</div>
                <div class="quck-right">
                    @auth('web')
                        <div class="right-link Tajawal-font"><a href="notifications.html"><i class="fa fa-bell" aria-hidden="true"></i>إشعاراتي</a></div>
                    @else
                        <div class="right-link Tajawal-font"><a href="login-register.html"><i class="fa  fa-user"></i>دخول / إنشاء حساب</a></div>
                    @endauth
                </div>
            </div>
        </div>
        <header id="header">
            <div class="container">
                <nav id="nav-main">
                    <div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <a href="index.html" class="navbar-brand"><img src="images/logo.png" alt=""></a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="sub-menu">
                                    <a class="Tajawal-font" href="{{ route('home') }}">الرئيسية</a>
                                </li>
                                <li class="sub-menu mega-menu">
                                    <a class="Tajawal-font" href="{{ route('about') }}">عن قياس2030</a>
                                </li>
                                <li class="mega-menu sub-menu">
                                    <a class="Tajawal-font" href="tests.html">إختبارات قياس</a>
                                    <div class="menu-view">
                                        <div class="row">
                                            @foreach($categories as $category)
                                                <div class="col-sm-3">
                                                    <div class="menu-title">{{ $category->cat_name }}</div>
                                                    @if($category->sub_categories->count())
                                                        <ul>
                                                            @foreach($category->sub_categories->take(4) as $sub_category)
                                                                <li>
                                                                    <a class="Tajawal-font" href="courses-gride.html">
                                                                        {{ $sub_category->cat_name }}
                                                                        <span class="muted-text">({{ $sub_category->exams->count() }} إختبار)</span>
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li><a class="Tajawal-font" href="courses-list-sideBar.html">الدورات التدريبية</a></li>
                                <li class="sub-menu mega-menu">
                                    <a class="Tajawal-font" href="books.html">الكتب الإلكترونية</a>
                                </li>
                                <li class="sub-menu mega-menu">
                                    <a class="Tajawal-font" href="{{ route('news.index') }}">أخبارنا</a>
                                </li>
                                <li class="sub-menu">
                                    <a class="Tajawal-font" href="video-liberary.html">مكتبة الفيديو</a>
                                </li>
                                <li class="sub-menu">
                                    <a class="Tajawal-font" href="contact-us.html">تواصل معنا</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>