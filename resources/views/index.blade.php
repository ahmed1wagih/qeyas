@extends('layouts.app')

@section('content')
    <section class="banner">
        <div class="banner-img">
            <img src="{{ asset('images/banner/banner-img11.jpg') }}" alt="">
        </div>
        <div class="banner-text">
            <div class="container">
                <div class="learning-btn">
                    <a href="#" class="btn">شاهد تفاصيل الإعلان</a>
                </div>
            </div>
        </div>
    </section>
    <section class="start-learning">
        <div class="container">
            <div class="holder">
                <ul id="ticker01">
                    <li>
                        <a href="#">
                            هذا النص هو مثال لنص يمكن أن يستبدل في نفس
                            المساحة، لقد تم توليد هذا النص من مولد النص
                            العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد
                            من النصوص الأخرى إضافة إلى زيادة عدد الحروف
                            التى يولدها التطبيق
                        </a>
                        <span>22 مايو 2019</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="our-tests">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="forum-details">
                        <div class="details-slide">
                            <div class="name">
                                <img src="{{ asset('images/user-img/test.png') }}" alt="">
                                <div class="Tajawal-font fontsize-18">الإختبارات التعليمية</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="btn-block">
                                <a href="tests.html" class="btn">المزيد</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="forum-details">
                        <div class="details-slide">
                            <div class="name">
                                <img src="{{ asset('images/user-img/test.png') }}" alt="">
                                <div class="Tajawal-font fontsize-18">الإختبارات التعليمية</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="btn-block">
                                <a href="tests.html" class="btn">المزيد</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="forum-details">
                        <div class="details-slide">
                            <div class="name">
                                <img src="{{ asset('images/user-img/test.png') }}" alt="">
                                <div class="Tajawal-font fontsize-18">الإختبارات التعليمية</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="info">
                                <div class="block fontsize-18"><a href="tests.html">قدرات(1)</a></div>
                                <div class="block">22 إختبار</div>
                            </div>
                            <div class="btn-block">
                                <a href="tests.html" class="btn">المزيد</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-course">
        <div class="container">
            <div class="section-title">
                <h2 class="Tajawal-font">أحدث الدورات التدريبية</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                                <div class="date"><i class="fa fa-clock-o"></i>22 ساعة</div>
                            </div>
                            <div class="price">$100</div>
                        </div>
                        <div class="course-name">مفاهيم تعليمية</div>
                        <div class="comment-row">
                            <div class="box"><i class="fa fa-user-secret"></i>أ/ محمد السيد</div>
                            <div class="enroll-btn">
                                <a href="#" class="btn">إشتراك</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                                <div class="date"><i class="fa fa-clock-o"></i>22 ساعة</div>
                            </div>
                            <div class="price">$100</div>
                        </div>
                        <div class="course-name">مفاهيم تعليمية</div>
                        <div class="comment-row">
                            <div class="box"><i class="fa fa-user-secret"></i>أ/ محمد السيد</div>
                            <div class="enroll-btn">
                                <a href="#" class="btn">إشتراك</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                                <div class="date"><i class="fa fa-clock-o"></i>22 ساعة</div>
                            </div>
                            <div class="price">$100</div>
                        </div>
                        <div class="course-name">مفاهيم تعليمية</div>
                        <div class="comment-row">
                            <div class="box"><i class="fa fa-user-secret"></i>أ/ محمد السيد</div>
                            <div class="enroll-btn">
                                <a href="#" class="btn">إشتراك</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-team Toplist">
        <div class="section-title">
            <h2 class="Tajawal-font">قائمة المتميزين</h2>
        </div>
        <div class="member-slider">
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
            <div class="event-box">
                <div class="price">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span>1</span>
                </div>
                <div class="img text-center"><img src="{{ asset('images/event/img1.jpg') }}" alt=""></div>
                <div class="event-name"><a href="#">محمد احمد احمد</a></div>
                <div class="event-info"><i class="fa fa-clock-o"></i> 15:22:10 دقيقة</div>
                <div class="event-info"><i class="fa fa-map-marker"></i>المنصورة</div>
                <div class="event-info"><i class="fa fa-percent"></i>80%</div>
            </div>
        </div>
    </section>
    <section class="our-course our-books">
        <div class="container">
            <div class="section-title">
                <h2 class="Tajawal-font">أحدث الكتب المضافة</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                            </div>
                        </div>
                        <div class="course-name Tajawal-font">اسم الكتاب
                            <span class="Tajawal-font">
                                هذا النص هو مثال لنص يمكن أن يستبدل في نفس
                                المساحة، لقد تم توليد هذا النص من مولد النص العربى
                            </span>
                        </div>
                        <div class="comment-row">
                            <div class="box"></div>
                            <div class="enroll-btns">
                                <a href="preview-book.html" class="btn">تصفح الكتاب</a>
                                <a href="preview-book.html" class="btn3"><i class="fa fa-download" aria-hidden="true"></i> تحميل الكتاب</a>
                            </div>
                            <div class="div-box">dwdwdw</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                            </div>
                        </div>
                        <div class="course-name Tajawal-font">اسم الكتاب
                            <span class="Tajawal-font">
                                هذا النص هو مثال لنص يمكن أن يستبدل في نفس
                                المساحة، لقد تم توليد هذا النص من مولد النص العربى
                            </span>
                        </div>
                        <div class="comment-row">
                            <div class="box"></div>
                            <div class="enroll-btns">
                                <a href="preview-book.html" class="btn">تصفح الكتاب</a>
                                <a href="preview-book.html" class="btn3"><i class="fa fa-download" aria-hidden="true"></i> تحميل الكتاب</a>
                            </div>
                            <div class="div-box">dwdwdw</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="course-box">
                        <div class="img">
                            <img src="{{ asset('images/courses/courses-img1.jpg') }}" alt="">
                            <div class="course-info">
                                <div class="date"><i class="fa fa-calendar"></i>22 مايو 2019</div>
                            </div>
                        </div>
                        <div class="course-name Tajawal-font">اسم الكتاب
                            <span class="Tajawal-font">
                                هذا النص هو مثال لنص يمكن أن يستبدل في نفس
                                المساحة، لقد تم توليد هذا النص من مولد النص العربى
                            </span>
                        </div>
                        <div class="comment-row">
                            <div class="box"></div>
                            <div class="enroll-btns">
                                <a href="preview-book.html" class="btn">تصفح الكتاب</a>
                                <a href="preview-book.html" class="btn3"><i class="fa fa-download" aria-hidden="true"></i> تحميل الكتاب</a>
                            </div>
                            <div class="div-box">dwdwdw</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="start-learning">
        <div class="container">
            <div class="holder text-center">
                <p><img src="{{ asset('images/test.svg') }}" alt="">
                    22 إختبار</p>
                <p><img src="{{ asset('images/course.svg') }}" alt=""> 239 دورة تدريبية</p>
                <p><img src="{{ asset('images/book.svg') }}" alt=""> 111 كتاب إلكتروني</p>
                <p><img src="{{ asset('images/user.svg') }}" alt=""> 1872 مستخدم</p>
            </div>
        </div>
    </section>
    <section class="contact-block">
        <div class="contact-form">
            <div class="section-title">
                <h2 class="Tajawal-font">تواصل معنا</h2>
            </div>
            <div class="form-filde">
                <form action="thank-you.html" method="post">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-box">
                                <input type="text" placeholder="الإسم" data-validation="required" name="name">
                            </div>
                            <div class="input-box">
                                <input type="text" placeholder="رقم الهاتف" data-validation="required" name="phone">
                            </div>
                            <div class="input-box">
                                <select class="form-control" data-validation="required" name="subject">
                                    <option selected>سبب الإتصال</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="input-box">
                                <textarea placeholder="محتوى الرسالة" data-validation="required" name="message"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="submit-box">
                                <input type="submit" value="إرسال" class="btn">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="map" id="map">
        </div>
    </section>
    <section>
        <button class="open-button" onclick="openForm()"><i class="fa fa-commenting fontsize-30" aria-hidden="true"></i></button>

        <div class="chat-popup" id="myForm">
            <form action="" class="form-container">
                <div class="msg-header">
                    <h4>أهلا و سهلا بك في قياس2030</h4>
                    <p>نحن نرد على كل رسالة لذا لا تتردد في السؤال عن اي شئ</p>
                </div>
                <textarea placeholder="أكتب رسالتك هنا..." name="msg" required></textarea>
                <div class="msg-footer">
                    <button type="submit" class="btn">إرسال</button>
                    <button type="button" class="btn cancel" onclick="closeForm()">إغلاق</button>
                </div>
            </form>
        </div>
    </section>
@endsection