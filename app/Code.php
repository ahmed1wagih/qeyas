<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Code
 *
 * @property string $email
 * @property string $token
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Code whereToken($value)
 * @mixin \Eloquent
 */
class Code extends Model
{
    public $timestamps = false;
    protected $table = 'password_resets';
    protected $fillable =
        [
            'phone','code','expire_at'
        ];
}
