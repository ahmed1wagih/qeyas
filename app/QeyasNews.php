<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\QeyasNews
 *
 * @property int $id
 * @property string $title
 * @property string $photo
 * @property string $content
 * @property string $news_date
 * @property string $num_watches
 * @property string $likes
 * @property string $dislikes
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereDislikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereNewsDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereNumWatches($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QeyasNews whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class QeyasNews extends Model
{
    //
}
