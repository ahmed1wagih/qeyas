<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ContactUs
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $subject
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactUs extends Model
{
    protected $table = 'contact_us';
    protected $fillable =
        [
            'name','phone','subject','message'
        ];
}
