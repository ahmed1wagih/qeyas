<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CourseRequest
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_phone
 * @property string $status
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereUserEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseRequest whereUserPhone($value)
 * @mixin \Eloquent
 */
class CourseRequest extends Model
{
    protected $table = 'course_requests';
}
