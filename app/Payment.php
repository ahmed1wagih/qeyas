<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Payment
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property int|null $book_id
 * @property int|null $exam_id
 * @property string $product_type
 * @property string $username
 * @property string $user_phone
 * @property string $paid_price
 * @property string $payment_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePaidPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUsername($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{

    protected static function boot() {
        parent::boot();
        self::creating(function($model){
            if(is_null(request('user_id'))){
                if(Auth::check()){
                    $model->user_id = Auth::id();
                } else {
                    $model->user_id = 0;
                }
            }
            if(is_null(request('category_id'))){
                $model->category_id = 0;
            }
        });
    }

}
