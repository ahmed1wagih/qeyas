<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property int $id
 * @property int $order
 * @property string $course_title
 * @property string $course_photo
 * @property string $start_date
 * @property string $end_date
 * @property string $hours
 * @property string $course_price
 * @property string $course_description
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCourseDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCoursePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCoursePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCourseTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Course extends Model
{
}
