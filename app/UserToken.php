<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $fillable =
        [
            'user_id','token'
        ];


    public static function send($tokens,$title,$msg,$type)
    {
        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [
                'type' => $type,
                'message' => $msg,
                'title' => $title,
            ],
            'notification' => [
                'type' => $type,
                'message' => $msg,
                'title' => $title,
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            'AAAA-oBKm98:APA91bHjGL6S8WENhwmYd9bRx1XRhkfv60OznFpz78TojG0j7vY-Z_RRBoFMkT75B6V6gUu23jKBsAqtcZKSOLDSkTZRhTHP2f9wrv-CmLv-wES6m6aHCJD5V5Iwheu2ukfNhHgSgirS'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }

}
