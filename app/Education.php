<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Education
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Education newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Education newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Education query()
 * @mixin \Eloquent
 */
class Education extends Model
{
    protected $table = 'educations';
    public $timestamps = false;
}
