<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamQuestion
 *
 * @property int $id
 * @property int $exam_id
 * @property string $question
 * @property int|null $right_answer_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereRightAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ExamQuestion extends Model
{
    //
}
