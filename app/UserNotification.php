<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserNotification
 *
 * @property int $id
 * @property string $user_gender
 * @property int $city_id
 * @property string $education_level
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereEducationLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserNotification whereUserGender($value)
 * @mixin \Eloquent
 */
class UserNotification extends Model
{
    //
}
