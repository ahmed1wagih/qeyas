<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamTryArchive extends Model
{
    protected $table = 'exam_try_archives';


    protected $fillable =
        [
            'try_id','question_id','answer_id','right_answer_id'
        ];



    public function get_question($id)
    {
        return Question::find($id)->question_text;
    }


    public function get_answers($id)
    {
        return Answer::where('question_id', $id)->select('id','option_text as text')->get();
    }
}
