<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Book
 *
 * @property int $id
 * @property string $book_title
 * @property string $book_photo
 * @property string $book_description
 * @property string $book_price
 * @property string $book_release_date
 * @property string $book_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookReleaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Book extends Model
{
    protected $table = 'books';
}
