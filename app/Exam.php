<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Exam
 *
 * @property int $id
 * @property string $title
 * @property int $course_id
 * @property int $category_id
 * @property int $available
 * @property string $exam_type
 * @property string $exam_price
 * @property string $exam_duration
 * @property string|null $icon_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereExamDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereExamPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereExamType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereIconUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exam whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Exam extends Model
{

    protected static function boot() {
        parent::boot();
        self::creating(function($model){
            if(is_null(request('exam_price')) || request('exam_type') === 'free'){
                $model->exam_price = 0;
            }
            if(is_null(request('course_id'))){
                $model->course_id = 0;
            }
        });
        self::updating(function($model){
            if(is_null(request('exam_price')) || request('exam_type') === 'free'){
                $model->exam_price = 0;
            }
            if(is_null(request('course_id'))){
                $model->course_id = 0;
            }
        });
    }


    public function is_purchased($exam_id,$user_id)
    {
        return boolval(ExamRequest::where('exam_id', $exam_id)->where('user_id', $user_id)->where('status','approved')->first());
    }


    public static function highest_attempt($exam_id,$user_id)
    {
        $attempt = ExamTry::where('user_id', $user_id)->orderBy('percentage','desc')->select('exam_id','result','time_spent','created_at')->first();
        $attempt['count'] = ExamTry::where('user_id',$user_id)->where('exam_id',$exam_id)->count();

        return $attempt;
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function questions() {
        return $this->hasMany(ExamQuestion::class);
    }
}
