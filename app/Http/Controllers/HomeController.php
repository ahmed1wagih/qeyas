<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index() {
        return view('index');
    }

    public function about() {
        return view('about');
    }

    public function news_index() {
        $news = News::where('active', 1)->paginate(1);
        return view('news.index', compact('news'));
    }

    public function news_show(News $news) {
        return view('news.show', compact('news'));
    }
}
