<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::where('cat_parent', 0)->orderBy('cat_order','asc')->select('id','cat_name','cat_order','cat_image')->get();
        foreach($categories as $cat)
        {
            $cat['exams_count'] = $cat->exams_count($cat->id);
            $cat['is_last'] = $cat->is_last($cat->id);
        }

        return response()->json($categories);
    }


    public function sub_cats($id)
    {
        $categories = Category::where('cat_parent', $id)->orderBy('cat_order', 'asc')->select('id','cat_name','cat_order','cat_image')->get();
        foreach($categories as $cat)
        {
            $cat['is_last'] = $cat->is_last($cat->id);
            if($cat->is_last == true)
            {
                $cat['exams_count'] = $cat->get_exams_count($cat->id);
            }
            else
            {
                $cat['exams_count'] = $cat->exams_count($cat->id);
            }
        }

        return response()->json($categories);
    }


    public function get_settings()
    {
        $settings = Setting::where('group','app')->select('key','display_name','value','details','type')->get();
        return response()->json($settings);
    }
}
