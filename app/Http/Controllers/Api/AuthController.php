<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Code;
use App\Education;
use App\Exam;
use App\ExamTry;
use App\User;
use App\UserToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'phone' => 'required',
                'gender' => 'sometimes|in:m,f',
                'city' => 'required|exists:cities,id',
                'education_level' => 'required|exists:educations,id',
                'password' => 'required',
                'token' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $phone_check = User::where('phone', $request->phone)->first();
        if($phone_check) return response()->json(['status' => 'error', 'msg' => 'phone exists']);

        $request->merge(
            [
                'password' => Hash::make($request->password)
            ]
        );


        $user = User::create($request->all());
        $user['city_name'] = isset($user->city) ? City::where('id', $user->city)->first()->city_name : '';
        $user['education_name'] = isset($user->education_level) ? Education::where('id', $user->education_level)->first()->name : '';
        $user['avatar'] = $user->fresh()->avatar;

        UserToken::updateOrcreate
        (
            [
                'user_id' => $user->id,
                'token' => $request->token
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'registered', 'user' => $user]);
    }


    public function social_login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'platform' => 'required|in:facebook,twitter,snapchat',
                'social_id' => 'required',
                'avatar' => 'required',
                'name' => 'required',
                'token'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }


        User::updateOrcreate
        (
            [
                'platform' => $request->platform,
                'social_id' => $request->social_id
            ],
            [
                'name' => $request->name,
                'avatar' => $request->avatar
            ]
        );

        $user = User::where('social_id',$request->social_id)->where('platform',$request->platform)->select('id','name','phone','city','education_level','avatar','password')->first();

        $user['city_name'] = $user->city != '' ? City::where('id', $user->city)->first()->city_name : '';
        $user['education_name'] = $user->education_level != '' ? Education::where('id', $user->education_level)->first()->name : '';

        UserToken::updateOrcreate
        (
            [
                'user_id' => $user->id,
                'token' => $request->token
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'logged in', 'user' => $user]);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone' => 'required',
                'password' => 'required',
                'token' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }


        $user = User::where('phone',$request->phone)->select('id','name','phone','city','education_level','avatar','gender','password')->first();

        if($user)
        {
            $check = Hash::check($request->password,$user->password);

            if($check)
            {
                $user['city_name'] = isset($user->city) ? City::where('id', $user->city)->first()->city_name : '';
                $user['education_name'] = isset($user->education_level) ? Education::where('id', $user->education_level)->first()->name : '';


                UserToken::updateOrcreate
                (
                    [
                        'user_id' => $user->id,
                        'token' => $request->token
                    ]
                );

                unset($user->password);
                return response()->json(['status' => 'success', 'msg' => 'logged in', 'user' => $user]);
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'invalid password']);
            }
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid phone']);
        }
    }


    public function token_update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'token' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        UserToken::updateOrcreate
        (
            [
                'user_id' => $request->user_id,
                'token' => $request->token
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'updated']);
    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'name' => 'required',
                'phone' => 'required',
                'gender' => 'sometimes|in:m,f',
                'city' => 'required|exists:cities,id',
                'education_level' => 'required',
                'password' => 'sometimes'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $phone_check = User::where('phone', $request->phone)->where('id','!=',$request->user_id)->first();
        if($phone_check) return response()->json(['status' => 'error', 'msg' => 'phone exists']);

        if($request->password)
        {
            $request->merge(['password' => Hash::make($request->password)]);
        }

        User::where('id', $request->user_id)->update($request->except('user_id'));

        $user = User::where('id', $request->user_id)->select('id','name','phone','city','education_level','avatar','gender')->first();
        $user['city_name'] = isset($user->city) ? City::where('id', $user->city)->first()->city_name : '';
        $user['education_name'] = isset($user->education_level) ? Education::where('id', $user->education_level)->first()->name : '';

        return response()->json(['status' => 'success','msg' => 'updated', 'user' => $user]);
    }



    public function password_reset(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $phone_check = User::where('phone', $request->phone)->first();
        if(!$phone_check) return response()->json(['status' => 'error', 'msg' => 'phone exists']);

        Code::UpdateOrCreate
        (
            [
                'phone' => $request->phone
            ],
            [
                'code' => rand(1000,9999),
                'expire_at' => Carbon::now()->addHour()
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'code sent']);
    }


    public function code_check(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'code' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $phone = Code::where('code', $request->code)->where('expire_at','>=', Carbon::now())->first();

        if($phone)
        {
            $user = User::where('phone', $phone->phone)->select('id')->first();
            return response()->json(['status' => 'success', 'msg' => 'code matched', 'user' => $user]);
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid code']);
        }

    }


    public function password_change(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'password' => 'required|min:6'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        User::where('id', $request->user_id)->update
        (
            [
                'password' => Hash::make($request->password)
            ]
        );

        return response()->json(['status' => 'success', 'msg' =>'password updated successfully']);
    }


    public function attempts(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $attempts = ExamTry::where('user_id', $request->user_id)->orderBy('percentage','desc')->select('exam_id','result','time_spent','created_at')->get()->unique('exam_id');

        foreach($attempts as $attempt)
        {
            $attempt['title'] = Exam::where('id', $attempt->exam_id)->select('title')->first()->title;
            $attempt['count'] = ExamTry::where('user_id',$request->user_id)->where('exam_id',$attempt->exam_id)->count();
        }

        return response()->json($attempts);
    }


    public function attempt_details(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'exam_id' => 'required|exists:exams,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $exam = Exam::where('id', $request->exam_id)->select('title')->first();
        $exam['highest_attempt'] = Exam::highest_attempt($request->exam_id,$request->user_id);

        $attempts = ExamTry::where('user_id', $request->user_id)->where('exam_id', $request->exam_id)->orderBy('percentage','desc')->select('result','time_spent','num_passed_questions','num_failed_questions','created_at')->latest()->get();

        return response()->json(['exam' => $exam, 'attempts' => $attempts]);
    }
}


