<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Education;
use App\Exam;
use App\ExamReport;
use App\ExamRequest;
use App\ExamTry;
use App\ExamTryArchive;
use App\Question;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    public function get_exams($cat_id,$user_id)
    {
        $exams = Exam::where('category_id', $cat_id)->where('available', 1)->select('id','title','exam_type','exam_price','exam_duration')->paginate(20);

        foreach($exams as $exam)
        {
            $exam['questions'] = Question::where('exam_id', $exam->id)->count();
            $exam['is_purchased'] = $exam->is_purchased($exam->id,$user_id);
        }

        return response()->json($exams);
    }


    public function get_exam(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'exam_id' => 'required|exists:exams,id'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $type = Exam::where('id', $request->exam_id)->select('exam_type')->first()->exam_type;

        if($type == 'paid')
        {
            $check = ExamRequest::where('user_id', $request->user_id)->where('exam_id', $request->exam_id)->where('status','approved')->first();

            if($check == null)
            {
                return response()->json(['status' => 'error', 'msg' => 'exam request not found']);
            }
        }

        $questions = Question::where('exam_id', $request->exam_id)->select('id','question_text as question','right_answer_id')->get();

        foreach($questions as $question)
        {
            $question['answers'] = $question->get_answers($question->id);
        }

        return response()->json($questions);
    }


    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id|exists:exam_requests,user_id,status,approved,exam_id,'.$request->exam_id,
                'exam_id' => 'required|exists:exams,id',
                'time_spent' => 'required',
                'num_passed_questions' => 'required',
                'num_failed_questions' => 'required',
                'answers' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }


        $check = Question::where('exam_id', $request->exam_id)->count();
        $total = $request->num_passed_questions + $request->num_failed_questions;

        if($check != $total)
        {
            return response()->json(['status' => 'error', 'msg' => 'questions count is invalid']);
        }

        $total = $request->num_passed_questions + $request->num_failed_questions;
        $request->merge
        (
            [
                'result' => $request->num_passed_questions.'/'.$total,
                'percentage' => round($request->num_passed_questions / $total * 100)
            ]
        );

        $try = ExamTry::create($request->except('answers'));

        foreach(explode('|',$request->answers) as $answer)
        {
            $answer = explode(',',$answer);

            ExamTryArchive::create
            (
                [
                    'try_id' => $try->id,
                    'question_id' => $answer[0],
                    'answer_id' => $answer[1],
                    'right_answer_id' => $answer[2],
                ]
            );
        }

        return response()->json(['status' => 'success', 'msg' => 'entry submitted successfully']);
    }


    public function previous_attempts(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $ids = ExamTry::where('user_id', $request->user_id)->pluck('exam_id');

        $exams = Exam::whereIn('id', $ids)->select('id','title')->get();

        foreach($exams as $exam)
        {
            $attempts = ExamTry::where('user_id', $request->user_id)->where('exam_id', $exam->id);

            $times = $attempts->pluck('time_spent');
            $total = 0;

            foreach ($times as $time)
            {
                $explode = explode(':',$time);

                $seconds = $explode[0] * 60 *60;
                $seconds += $explode[1] * 60;
                $seconds += $explode[2];

                $total += $seconds;
            }

            $exam['highest_result'] = $attempts->orderBy('percentage','desc')->first()->result;
            $exam['count'] = $attempts->count();
            $exam['total_spent'] = gmdate('H:i:s', $total);
            $exam['latest'] = $attempts->latest()->first()->created_at->toDateTimeString();
        }

        return response()->json($exams);
    }


    public function get_attempts(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'exam_id' => 'required|exists:exams,id'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $attempts = ExamTry::where('user_id', $request->user_id)->where('exam_id', $request->exam_id)->select('id','result','percentage','time_spent','num_passed_questions','num_failed_questions','created_at as date')->orderBy('date','desc')->get();

        $total = 0;

        foreach ($attempts as $attempt)
        {
            $explode = explode(':',$attempt->time_spent);

            $seconds = $explode[0] * 60 *60;
            $seconds += $explode[1] * 60;
            $seconds += $explode[2];

            $total += $seconds;
        }


        $data['title'] = Exam::where('id', $request->exam_id)->select('title')->first()->title;
        $data['count'] = $attempts->count();
        $data['total_spent'] = gmdate('H:i:s', $total);
        $data['latest'] = $attempts->first()->date;

        return response()->json(['data' => $data,'attempts' => $attempts]);
    }


    public function get_attempt_archive(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'attempt_id' => 'required|exists:exam_tries,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }

        $questions = ExamTryArchive::where('try_id', $request->attempt_id)->select('question_id','answer_id','right_answer_id')->get();

        foreach($questions as $question)
        {
            $question['question'] = $question->get_question($question->question_id);
            $question['answers'] = $question->get_answers($question->question_id);

            unset($question->question_id);
        }

        return response()->json($questions);
    }


    public function the_best(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'exam_id' => 'required|exists:exams,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->getMessageBag()]);
        }


        $bests = ExamTry::where('exam_id', $request->exam_id)->orderBy('percentage','desc')->select('user_id as user','result','time_spent','percentage')->paginate(1);
        $n = 1;

        foreach($bests as $best)
        {

            $best['order'] = $n;
            $best['user'] = $best->get_user($best->user);

            $n++;
        }

        return response()->json($bests);
    }
}
