<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('about-us', 'HomeController@about')->name('about');
Route::get('news', 'HomeController@news_index')->name('news.index');
Route::get('news/{news}', 'HomeController@news_show')->name('news.show');